package com.apollo.springrest.inventory;

import com.apollo.springrest.inventory.service.InventoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestWithSpringInventoryApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private InventoryService inventoryService;

	@Test
	public void test1()throws Exception{

		assert inventoryService.getAllRoomsWithCategory(1) != null;

	}

	@Test
	public void testGetRoom()throws Exception{
		assert inventoryService.getRoom(1) != null;
		System.out.println(inventoryService.getRoom(1).getName());
	}

}
