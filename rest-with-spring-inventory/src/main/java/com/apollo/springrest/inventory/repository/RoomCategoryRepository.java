package com.apollo.springrest.inventory.repository;

import com.apollo.springrest.inventory.domain.RoomCategory;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by nikif on 15/04/2017.
 */
public interface RoomCategoryRepository extends CrudRepository<RoomCategory, Integer>{

}
