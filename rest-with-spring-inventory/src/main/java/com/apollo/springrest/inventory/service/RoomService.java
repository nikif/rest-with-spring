package com.apollo.springrest.inventory.service;

import com.apollo.springrest.inventory.domain.Room;

public interface RoomService {

    Room getRoom(Integer id);
}
