package com.apollo.springrest.inventory.service;

import com.apollo.springrest.inventory.domain.Room;
import com.apollo.springrest.inventory.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    RoomRepository roomRepository;

    @Autowired
    public RoomServiceImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public Room getRoom(Integer id) {
        return roomRepository.findOne(id);
    }

}
