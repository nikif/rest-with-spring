package com.apollo.springrest.inventory.domain;

import javax.persistence.*;

@Entity(name = "pricings")
public class Pricing {

    @Id
    @GeneratedValue
    private Integer id;

    @Enumerated(EnumType.STRING)
    private PricingModel pricingModel;

    @Column(nullable = false)
    private Double priceGuest1;
    @Column(nullable = false)
    private Double priceGuest2;
    @Column(nullable = false)
    private Double priceGuest3;

    @OneToOne
    private RoomCategory roomCategory;

    public Pricing() {
    }

    public Pricing(PricingModel pricingModel, Double priceGuest1, Double priceGuest2, Double priceGuest3) {
        this.pricingModel = pricingModel;
        this.priceGuest1 = priceGuest1;
        this.priceGuest2 = priceGuest2;
        this.priceGuest3 = priceGuest3;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PricingModel getPricingModel() {
        return pricingModel;
    }

    public void setPricingModel(PricingModel pricingModel) {
        if (pricingModel == null){
            throw new IllegalArgumentException("pricing model setter");
        }
        this.pricingModel = pricingModel;
    }

    public Double getPriceGuest1() {
        return priceGuest1;
    }

    public void setPriceGuest1(Double priceGuest1) {
        this.priceGuest1 = priceGuest1;
    }

    public Double getPriceGuest2() {
        return priceGuest2;
    }

    public void setPriceGuest2(Double priceGuest2) {
        this.priceGuest2 = priceGuest2;
    }

    public Double getPriceGuest3() {
        return priceGuest3;
    }

    public void setPriceGuest3(Double priceGuest3) {
        this.priceGuest3 = priceGuest3;
    }

    @Override
    public String toString() {
        return "Pricing{" +
                "id=" + id +
                ", pricingModel=" + pricingModel +
                ", priceGuest1=" + priceGuest1 +
                ", priceGuest2=" + priceGuest2 +
                ", priceGuest3=" + priceGuest3 +
                '}';
    }
}
