package com.apollo.springrest.inventory.bootstrap;

import com.apollo.springrest.inventory.domain.Pricing;
import com.apollo.springrest.inventory.domain.PricingModel;
import com.apollo.springrest.inventory.domain.Room;
import com.apollo.springrest.inventory.domain.RoomCategory;
import com.apollo.springrest.inventory.repository.PricingRepository;
import com.apollo.springrest.inventory.repository.RoomCategoryRepository;
import com.apollo.springrest.inventory.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner{

    private final RoomRepository roomRepository;
    private final RoomCategoryRepository roomCategoryRepository;
    private final PricingRepository pricingRepository;

    @Autowired
    public DatabaseLoader(RoomRepository roomRepository, RoomCategoryRepository roomCategoryRepository, PricingRepository pricingRepository) {
        this.roomRepository = roomRepository;
        this.roomCategoryRepository = roomCategoryRepository;
        this.pricingRepository = pricingRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        Pricing pricing1 = pricingRepository.save(new Pricing(PricingModel.FIXED, 50.0, 50.0, 50.0));
        pricingRepository.save(new Pricing(PricingModel.SLIDING, 50.0, 100.0, 150.0));

        RoomCategory roomCategory1 = roomCategoryRepository.save(new RoomCategory("single ocean bed room","a single bed room with view", pricing1));

        roomRepository.save(new Room(roomCategory1, "Floor 1 Room 1", "single bed on the first floor with ocean view"));
        roomRepository.save(new Room(roomCategory1, "Floor 1 Room 2", "single bed on the first floor with mountain view"));

    }
}
