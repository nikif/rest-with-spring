package com.apollo.springrest.inventory.domain;

import javax.persistence.*;

@Entity(name = "rooms")
public class Room {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    private RoomCategory roomCategory;

    @Column(name = "name", unique = true, nullable = false, length = 128)
    private String name;
    private String description;

    // constructor needed for hibernate
    public Room() {
    }

    public Room(RoomCategory roomCategory, String name, String description) {
        this.roomCategory = roomCategory;
        this.name = name;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoomCategory getRoomCategory() {
        return roomCategory;
    }

    public void setRoomCategory(RoomCategory roomCategory) {
        this.roomCategory = roomCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", roomCategory=" + roomCategory +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
