package com.apollo.springrest.inventory.domain;

public enum PricingModel {
    FIXED,
    SLIDING
}
