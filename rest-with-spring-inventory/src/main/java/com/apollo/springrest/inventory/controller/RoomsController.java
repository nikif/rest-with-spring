package com.apollo.springrest.inventory.controller;

import com.apollo.springrest.inventory.domain.Room;
import com.apollo.springrest.inventory.dto.RoomDTO;
import com.apollo.springrest.inventory.service.InventoryService;
import com.apollo.springrest.inventory.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rooms")
public class RoomsController {

    private final RoomService roomService;
    private final InventoryService inventoryService;

    @Autowired
    public RoomsController(RoomService roomService, InventoryService inventoryService) {
        this.inventoryService = inventoryService;
        this.roomService = roomService;
    }

    @RequestMapping(value = "/{roomId}", method = RequestMethod.GET)
    public @ResponseBody RoomDTO getRoom(@PathVariable("roomId") Integer roomId){
        Room room = roomService.getRoom(roomId);
        return new RoomDTO(room);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<RoomDTO> getRoomsWithCategoryId(@RequestParam("categoryId") Integer categoryId){
        return inventoryService.getAllRoomsWithCategory(categoryId).stream().map(RoomDTO::new).collect(Collectors.toList());
    }

    //  #todo create a method to create rooms with post requests
    // @RequestMapping(method = RequestMethod.POST)


}
