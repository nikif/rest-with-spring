package com.apollo.springrest.inventory.service;

import com.apollo.springrest.inventory.domain.Room;
import com.apollo.springrest.inventory.domain.RoomCategory;
import com.apollo.springrest.inventory.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by nikif on 16/04/2017.
 */
@Service
public class InventoryServiceImpl implements InventoryService {

    RoomRepository roomRepository;

    @Autowired
    public InventoryServiceImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public List<Room> getAllRoomsWithCategory(Integer id) {
        return roomRepository.findAllByRoomCategoryId(id);
    }

    @Override
    public Room getRoom(Integer id) {
        return roomRepository.findRoomById(id);
    }
}
