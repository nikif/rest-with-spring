package com.apollo.springrest.inventory.dto;


import com.apollo.springrest.inventory.domain.Room;

import java.io.Serializable;

// its just an envelope filter container for our class
// for example we do not want to share same information
public class RoomDTO implements Serializable{

    private static final long serialVersionUID = 2682046985632747474L;

    private Integer id;
    private String name;
    private long roomCategoryId;
    private String description;

    public RoomDTO(Room room) {
        this.id = room.getId();
        this.name = room.getName();
        this.roomCategoryId = room.getRoomCategory().getId();
        this.description = room.getDescription();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getRoomCategoryId() {
        return roomCategoryId;
    }

    public void setRoomCategoryId(long roomCategoryId) {
        this.roomCategoryId = roomCategoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
