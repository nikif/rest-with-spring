package com.apollo.springrest.inventory.repository;

import com.apollo.springrest.inventory.domain.Room;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoomRepository extends CrudRepository<Room, Integer> {

    List<Room> findAllByRoomCategoryId(Integer id);

    Room findRoomById(Integer id);
}
