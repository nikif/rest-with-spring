package com.apollo.springrest.inventory.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class RoomCategory {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "name", unique = true, nullable = false, length = 128)
    private String name;

    @Column(name = "description", length = 2048)
    private String description;

    @OneToOne
    private Pricing pricing;

    // A room category belongs to many rooms
    // many rooms hava one roomcategory
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roomCategory")
    private List<Room> rooms = new ArrayList<>();

    public RoomCategory() {
    }

    public RoomCategory(String name, String description, Pricing pricing) {
        this.name = name;
        this.description = description;
        this.pricing = pricing;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Pricing getPricing() {
        return pricing;
    }

    public void setPricing(Pricing pricing) {
        this.pricing = pricing;
    }

    public void addRoom(Room room){
        this.rooms.add(room);
        room.setRoomCategory(this);
    }

    public void removeRoom(Room room){
            room.setRoomCategory(null);
            this.rooms.remove(room);
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @Override
    public String toString() {
        return "RoomCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", pricing=" + pricing +
                '}';
    }
}
