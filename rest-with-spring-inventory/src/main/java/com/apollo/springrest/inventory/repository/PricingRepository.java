package com.apollo.springrest.inventory.repository;

import com.apollo.springrest.inventory.domain.Pricing;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by nikif on 15/04/2017.
 */
public interface PricingRepository extends CrudRepository<Pricing, Integer> {
}
