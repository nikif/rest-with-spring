package com.apollo.springrest.inventory.service;

import com.apollo.springrest.inventory.domain.Room;
import com.apollo.springrest.inventory.domain.RoomCategory;

import java.util.List;

/**
 * Created by nikif on 16/04/2017.
 */
public interface InventoryService {

    Room getRoom(Integer id);

    List<Room> getAllRoomsWithCategory(Integer id);
}
